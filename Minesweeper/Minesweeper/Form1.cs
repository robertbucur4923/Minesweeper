﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minesweeper
{
    public partial class MinesweeperApp : Form
    {
        public MinesweeperApp()
        {
            InitializeComponent();
        }

        CheckBox[,] matr = new CheckBox[100, 100]; //
        Random rnd = new Random(); //

        private void Generate_Click(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(linesColumns.Text); //
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matr[i, j] = new CheckBox();
                    matr[i, j].Parent = this;
                    matr[i, j].Top = i * 50 + 100;
                    matr[i, j].Left = j * 50 + 100;
                    matr[i, j].Height = 30;
                    matr[i, j].Width = 30;
                    matr[i, j].Text = Convert.ToString(rnd.Next(0, 3));
                    matr[i, j].Font = new Font("Times New Roman", 12);
                    matr[i, j].Appearance = Appearance.Button;
                    matr[i, j].Visible = true;
                }
            } //
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (matr[i, j].Text == Convert.ToString(0))
                        matr[i, j].Text = Convert.ToString('*');
                }
            }//inloc 0 cu *
            for (int i = 1; i < n - 1; i++)
            {
                for (int j = 1; j < n - 1; j++)
                {
                    if (matr[i, j].Text != Convert.ToString('*'))
                    {
                        int nr = 1;
                        matr[i, j].Text = Convert.ToString(0);
                        if (matr[i - 1, j - 1].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);

                        if (matr[i - 1, j].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);

                        if (matr[i - 1, j + 1].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);

                        if (matr[i, j + 1].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);

                        if (matr[i + 1, j + 1].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);

                        if (matr[i + 1, j].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);

                        if (matr[i + 1, j - 1].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);

                        if (matr[i, j - 1].Text == Convert.ToString('*'))
                            matr[i, j].Text = Convert.ToString(nr++);
                    }
                }
            }

            //prima linie
            for (int j = 1; j < n - 1; j++)
            {
                if (matr[0, j].Text != Convert.ToString('*'))
                {
                    int nr = 1;
                    matr[0, j].Text = Convert.ToString(0);
                    if (matr[0, j + 1].Text == Convert.ToString('*'))
                        matr[0, j].Text = Convert.ToString(nr++);

                    if (matr[1, j + 1].Text == Convert.ToString('*'))
                        matr[0, j].Text = Convert.ToString(nr++);

                    if (matr[1, j].Text == Convert.ToString('*'))
                        matr[0, j].Text = Convert.ToString(nr++);

                    if (matr[1, j - 1].Text == Convert.ToString('*'))
                        matr[0, j].Text = Convert.ToString(nr++);

                    if (matr[0, j - 1].Text == Convert.ToString('*'))
                        matr[0, j].Text = Convert.ToString(nr++);
                }
            }
            //ultima linie
            for (int j = 1; j < n - 1; j++)
            {
                if (matr[n - 1, j].Text != Convert.ToString('*'))
                {
                    int nr = 1;
                    matr[n - 1, j].Text = Convert.ToString(0);
                    if (matr[n - 1, j - 1].Text == Convert.ToString('*'))
                        matr[n - 1, j].Text = Convert.ToString(nr++);

                    if (matr[n - 2, j - 1].Text == Convert.ToString('*'))
                        matr[n - 1, j].Text = Convert.ToString(nr++);

                    if (matr[n - 2, j].Text == Convert.ToString('*'))
                        matr[n - 1, j].Text = Convert.ToString(nr++);

                    if (matr[n - 2, j + 1].Text == Convert.ToString('*'))
                        matr[n - 1, j].Text = Convert.ToString(nr++);

                    if (matr[n - 1, j + 1].Text == Convert.ToString('*'))
                        matr[n - 1, j].Text = Convert.ToString(nr++);
                }
            }
            //ultima coloana
            for (int i = 1; i < n - 1; i++)
            {
                if (matr[i, n - 1].Text != Convert.ToString('*'))
                {
                    int nr = 1;
                    matr[i, n - 1].Text = Convert.ToString(0);
                    if (matr[i - 1, n - 1].Text == Convert.ToString('*'))
                        matr[i, n - 1].Text = Convert.ToString(nr++);

                    if (matr[i - 1, n - 2].Text == Convert.ToString('*'))
                        matr[i, n - 1].Text = Convert.ToString(nr++);

                    if (matr[i, n - 2].Text == Convert.ToString('*'))
                        matr[i, n - 1].Text = Convert.ToString(nr++);

                    if (matr[i + 1, n - 2].Text == Convert.ToString('*'))
                        matr[i, n - 1].Text = Convert.ToString(nr++);

                    if (matr[i + 1, n - 1].Text == Convert.ToString('*'))
                        matr[i, n - 1].Text = Convert.ToString(nr++);
                }
            }
            //prima coloana
            for (int i = 1; i < n - 1; i++)
            {
                if (matr[i, 0].Text != Convert.ToString('*'))
                {
                    int nr = 1;
                    matr[i, 0].Text = Convert.ToString(0);
                    if (matr[i - 1, 0].Text == Convert.ToString('*'))
                        matr[i, 0].Text = Convert.ToString(nr++);

                    if (matr[i - 1, 1].Text == Convert.ToString('*'))
                        matr[i, 0].Text = Convert.ToString(nr++);

                    if (matr[i, 1].Text == Convert.ToString('*'))
                        matr[i, 0].Text = Convert.ToString(nr++);

                    if (matr[i + 1, 1].Text == Convert.ToString('*'))
                        matr[i, 0].Text = Convert.ToString(nr++);

                    if (matr[i + 1, 0].Text == Convert.ToString('*'))
                        matr[i, 0].Text = Convert.ToString(nr++);
                }
            }

            /*matr[0, 0].Visible = false;
            matr[n - 1, 0].Visible = false;
            matr[n - 1, n - 1].Visible = false;
            matr[0, n - 1].Visible = false;*/

            //colturi
            /* matr[0, 0].Text = Convert.ToString(0);
             matr[n-1, 0].Text = Convert.ToString(0);
             matr[n-1, n-1].Text = Convert.ToString(0); 
             matr[0, n-1].Text = Convert.ToString(0);*/

            if (matr[0, n - 1].Text != Convert.ToString('*'))
            {
                int nr1 = 1;
                if (matr[1, n - 2].Text == Convert.ToString('*'))
                    matr[0, n - 1].Text = Convert.ToString(nr1++);
                if (matr[0, n - 2].Text == Convert.ToString('*'))
                    matr[0, n - 1].Text = Convert.ToString(nr1++);
                if (matr[1, n - 1].Text == Convert.ToString('*'))
                    matr[0, n - 1].Text = Convert.ToString(nr1++);

                if (nr1 == 1)
                    matr[0, n - 1].Text = Convert.ToString(0);
            }

            if (matr[0, 0].Text != Convert.ToString('*'))
            {
                int nr1 = 1;
                if (matr[0, 1].Text == Convert.ToString('*'))
                    matr[0, 0].Text = Convert.ToString(nr1++);
                if (matr[1, 1].Text == Convert.ToString('*'))
                    matr[0, 0].Text = Convert.ToString(nr1++);
                if (matr[1, 0].Text == Convert.ToString('*'))
                    matr[0, 0].Text = Convert.ToString(nr1++);

                if (nr1 == 1)
                    matr[0, 0].Text = Convert.ToString(0);
            }

            if (matr[n - 1, 0].Text != Convert.ToString('*'))
            {
                int nr1 = 1;
                if (matr[n - 2, 0].Text == Convert.ToString('*'))
                    matr[n - 1, 0].Text = Convert.ToString(nr1++);
                if (matr[n - 2, 1].Text == Convert.ToString('*'))
                    matr[n - 1, 0].Text = Convert.ToString(nr1++);
                if (matr[n - 1, 1].Text == Convert.ToString('*'))
                    matr[n - 1, 0].Text = Convert.ToString(nr1++);

                if (nr1 == 1)
                    matr[n - 1, 0].Text = Convert.ToString(0);
            }

            if (matr[0, 0].Text != Convert.ToString('*'))
            {
                int nr1 = 1;
                if (matr[n - 2, n - 2].Text == Convert.ToString('*'))
                    matr[n - 1, n - 1].Text = Convert.ToString(nr1++);
                if (matr[n - 2, n - 1].Text == Convert.ToString('*'))
                    matr[n - 1, n - 1].Text = Convert.ToString(nr1++);
                if (matr[n - 1, n - 2].Text == Convert.ToString('*'))
                    matr[n - 1, n - 1].Text = Convert.ToString(nr1++);

                if (nr1 == 1)
                    matr[n - 1, n - 1].Text = Convert.ToString(0);
            }
            
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void NewGame_Click(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(linesColumns.Text);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matr[i, j].Visible = false;
                }
            }
            linesColumns.Text = "0";
            Generate_Click(sender, e);
        }

        private void Help_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are presented with a board of squares. Some squares contain mines (bombs), others don't. If you click on a square containing a bomb, you lose. If you manage to click all the squares (without clicking on any bombs) you win. Clicking a square which doesn't have a bomb reveals the number of neighbouring squares containing bombs. Use this information plus some guess work to avoid the bombs. To open a square, point at the square and click on it. To mark a square you think is a bomb, point and right-click (or hover with the mouse and press Space).");
        }
    }
}
